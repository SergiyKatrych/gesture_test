﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PDollarGestureRecognizer;
using System.IO;
using System;

public class GesturesManager
{
	#region Singleton
	private static GesturesManager instance;

	public static GesturesManager Instance
	{
		get
		{
			if (instance == null)
			{

				instance = new GesturesManager();
			}
			return instance;
		}
	}
	#endregion

	private string pathToGestures;
	private List<Gesture> gestures = new List<Gesture>();

	public List<Gesture> Gestures
	{
		get
		{
			if (gestures == null)
			{
				gestures = new List<Gesture>();
			}
			return gestures;
		}
		set
		{
			gestures = value;
		}
	}

	//GUI
	private string message;
	private bool recognized;
	private string newGestureName = "";

	private GesturesManager()
	{
		pathToGestures = Application.persistentDataPath + "/Gestures";
		LoadGestures();
	}

	private void LoadGestures()
	{
		//Load user custom gestures
		try
		{
			string[] filePaths = Directory.GetFiles(pathToGestures, "*.xml");
			foreach (string filePath in filePaths)
			{
				Debug.Log("try to read "+filePath);
				gestures.Add(GestureIO.ReadGestureFromFile(filePath));
			}
		}
		catch (DirectoryNotFoundException e)
		{
			SaveDefaultGestures();
		}
	}

	private void SaveDefaultGestures()
	{
	}

	public void SaveGesture(Gesture gesture)
	{
		string fileName = String.Format("{0}/{1}.xml", pathToGestures, gesture.Name);

		#if !UNITY_WEBPLAYER
		GestureIO.WriteGesture(gesture.Points, gesture.Name, pathToGestures, fileName);
		#endif

		gestures.Add(gesture);
	}

	private void UpdateGesture(int gestureToEdit, Gesture gesture)
	{
		string fileName = String.Format("{0}/{1}.xml", pathToGestures, gestures[gestureToEdit].Name);
		File.Delete(fileName);

		#if !UNITY_WEBPLAYER
		GestureIO.WriteGesture(gesture.Points, gesture.Name, pathToGestures, fileName);
		#endif

		gestures[gestureToEdit] = gesture;
	}

	public void DeleteGesture(Gesture gesture)
	{
		string fileName = String.Format("{0}/{1}.xml", pathToGestures, gesture.Name);
		File.Delete(fileName);
		gestures.Remove(gesture);
	}

	public static Vector3[] GetAllVertices(Point[] points, Vector2 targetAreaSize)
	{
		Vector3[] result = new Vector3[points.Length];
		Vector2 tempResult = new Vector2();
		float minX = points[0].X, maxX = points[0].X, minY = points[0].Y, maxY = points[0].Y;
		for (int i = 0; i < points.Length; i++)
		{
			Point point = points[i];
			result[i] = new Vector3(point.X, point.Y, 10);
			tempResult += new Vector2(point.X, point.Y);
			if (tempResult.x < minX)
			{
				minX = tempResult.x;
			}
			if (tempResult.x > maxX)
			{
				maxX = tempResult.x;
			}
			if (tempResult.y < minY)
			{
				minY = tempResult.y;
			}
			if (tempResult.y > maxY)
			{
				maxY = tempResult.y;
			}
		}

		float koefX = targetAreaSize.x / Math.Abs(maxX - minX);
		float koefY = targetAreaSize.y / Math.Abs(maxY - minY);

		float koef = koefX < koefY ? koefY : koefX;

		for (int i = 0; i < result.Length; i++)
		{
			result[i] *= koef;
		}

		//Debug.Log("koefx " + koefX + " koefY " + koefY + " koef " + koef + "+min X " + minX + " max X " + maxX + " min Y " + minY + " maxY " + maxY);
		return result;
	}


}

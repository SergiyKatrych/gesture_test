﻿using UnityEngine;
using System.Collections;

public static class IDs
{
	public enum Scenes
	{
		MainMenu = 0,
		Gameplay = 1,
		Settings = 2
	}

	public static string GetNameString(this Scenes scene)
	{
		string result = null;
		switch (scene)
		{
			case Scenes.MainMenu:
				result = "MainMenu";
				break;
			case Scenes.Settings:
				result = "Settings";
				break;
			case Scenes.Gameplay:
				result = "Gameplay";
				break;
		}
		return result;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PDollarGestureRecognizer;
using System;

public class GestureInputController : MonoBehaviour
{
	private bool isWaitingForGesture = false;
	private bool isDrawingGesture = false;
	[SerializeField] ParticleSystem particleSystem;
	[SerializeField] int minimumUpdatesTillCount = 10;

	private List<Point> points = new List<Point>();

	private Vector3 virtualKeyPosition = Vector2.zero;
	private Rect drawArea;

	private RuntimePlatform platform;

	private List<LineRenderer> gestureLinesRenderer = new List<LineRenderer>();
	private LineRenderer currentGestureLineRenderer;


	private Action<Gesture> onGestureCallback;



	public void InitForInput(Action<Gesture> onGestureFinishedCallback)
	{
		onGestureCallback = onGestureFinishedCallback;
	}

	bool isIteruptedByRecognition = false;

	public void InitForRecognition(Action<Gesture> onGestureRecognizedCallback)
	{
		isIteruptedByRecognition = true;
		onGestureCallback = onGestureRecognizedCallback;
	}

	private void Init()
	{
		platform = Application.platform;
		drawArea = new Rect(0, 0, Screen.width - Screen.width / 3, Screen.height);
	}
		
	// Update is called once per frame
	void Update()
	{
		
		if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer)
		{
			if (Input.touchCount > 0)
			{
				virtualKeyPosition = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
			}
		}
		else
		{
			if (Input.GetMouseButton(0))
			{
				virtualKeyPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
			}
		}

		if (virtualKeyPosition.x == float.NaN || virtualKeyPosition.y == float.NaN)
		{
			return;
		}


		if (Input.GetMouseButtonDown(0))
		{

			if (isWaitingForGesture)
			{
				isDrawingGesture = true;
				isWaitingForGesture = false;


				Vector3 worldPos = virtualKeyPosition;
				worldPos.z = 10;
				worldPos = Camera.main.ScreenToWorldPoint(worldPos);
				particleSystem.transform.position = worldPos;


				var em = particleSystem.emission;
				em.enabled = true;


				particleSystem.Play();
			}
		}
			

		if (isDrawingGesture && Input.GetMouseButton(0))
		{


			Vector3 worldPos = virtualKeyPosition;
			worldPos.z = 10;
			worldPos = Camera.main.ScreenToWorldPoint(worldPos);

			points.Add(new Point(virtualKeyPosition.x, virtualKeyPosition.y, -1));
			particleSystem.transform.position = worldPos;
		}

		if (isDrawingGesture)
		{
			//if not right after the button pressed
			if (Input.GetMouseButtonUp(0) && points.Count > minimumUpdatesTillCount)
			{
				particleSystem.Stop();
				particleSystem.Clear();

				EndOfGesture();
				isDrawingGesture = false;
			}
		}
	}

	private void EndOfGesture()
	{
		
		isWaitingForGesture = false;
		Gesture candidate = new Gesture(points.ToArray());
		if (onGestureCallback != null)
		{
			var callback = onGestureCallback;
			onGestureCallback = null;
			callback(candidate);
		}
		//enabled = false;
	}

	public void GetGesture(Action<Gesture> onGestureFinishedCallback)
	{
		onGestureCallback = onGestureFinishedCallback;
		isWaitingForGesture = true;
		points = new List<Point>();

		enabled = true;
	}

	void OnEnable()
	{
		onGestureCallback = null;

		var em = particleSystem.emission;
		em.enabled = false;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using PDollarGestureRecognizer;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameplayUIController : DefaultScreenController
{
	

	#region implemented abstract members of DefaultScreenController
	public override void OnBackButtonPressed()
	{
		BackToMainMenu();
	}
	#endregion

	public void BackToMainMenu()
	{
		SceneManager.LoadScene(IDs.Scenes.MainMenu.GetNameString());
	}

	#region Gameplay
	[Header("References")]
	[SerializeField] private GestureInputController gestureInputController;
	[SerializeField] private RectTransform areaToShowGesture;
	[SerializeField] private LineRenderer targetGestureDrawer;

	[SerializeField] private Text scoreText;
	[SerializeField] private Text timeText;

	[SerializeField] private LosePanelController losePanel;


	[Header("Game balance")]
	[SerializeField] [Range(0, 10)] private float secondsPerGestureOnStart = 10;
	[SerializeField] [Range(0, 1)] private float nextRoundTimeDecreaseKoef = 0.75f;
	[SerializeField] [Range(0, 1)] private float acceptableDistortionLevel = 0.7f;


	private Vector2 gestureAreaSize;

	private List<Gesture> gestures;
	private Gesture currentGesture;

	private float currentTime = 0;
	private int score = 0;
	private bool isGameActive = false;


	private float levelDurationInSecs;

	void Start()
	{
		levelDurationInSecs = secondsPerGestureOnStart;

		currentTime = levelDurationInSecs;
		gestureAreaSize = areaToShowGesture.rect.size;
		gestureAreaSize = Camera.main.ScreenToWorldPoint(new Vector3(gestureAreaSize.x, gestureAreaSize.y, 10));

		gestureAreaSize *= 5;

		gestures = GesturesManager.Instance.Gestures;

		losePanel.gameObject.SetActive(false);
		gestureInputController.enabled = true;

		NextGesture();
		isGameActive = true;
	}

	public void Replay()
	{
		Start();
	}

	private void GameWon()
	{
		levelDurationInSecs *= nextRoundTimeDecreaseKoef;

		score++;
		NextGesture();

		currentTime = levelDurationInSecs;
	}

	private void GameLost()
	{
		gestureInputController.enabled = false;
		losePanel.Show(score);
	}

	public override void Update()
	{
		base.Update();
		if (isGameActive && currentTime > 0)
		{
			currentTime -= Time.deltaTime;
			if (currentTime < 0)
			{
				currentTime = 0;
			}
			UpdateTexts();

		}
		else
		{
			GameLost();
		}
	}

	private void UpdateTexts()
	{
		scoreText.text = score.ToString();
		timeText.text = currentTime.ToString("F");
	}

	private void NextGesture()
	{
		currentGesture = GetRandomGesture();

		DrawTargetGesture();

		GetUserGesture();
	}

	private void GetUserGesture()
	{
		gestureInputController.GetGesture(
			(result) =>
			{
				if (isGameActive)
				{
					if (CompareUsersGesture(result))
					{
					
						GameWon();
					}
					else
					{
						GetUserGesture();
					}
				}
			}
		);
	}

	private bool CompareUsersGesture(Gesture userGesture)
	{
		Result gestureResult = PointCloudRecognizer.Classify(userGesture, currentGesture);
		if (gestureResult.GestureClass == currentGesture.Name && gestureResult.Score > acceptableDistortionLevel)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private void DrawTargetGesture()
	{
		Vector3[] targetPoints = GesturesManager.GetAllVertices(currentGesture.Points, gestureAreaSize);
		targetGestureDrawer.SetVertexCount(targetPoints.Length);
		targetGestureDrawer.SetPositions(targetPoints);
	}

	private Gesture GetRandomGesture()
	{
		Gesture result = null;

		if (gestures.Count < 1)
		{
			return currentGesture;
		}

		while (result == null || result == currentGesture)
		{
			result = gestures[Random.Range(0, gestures.Count)];
		}

		return result;
	}

	#endregion
}

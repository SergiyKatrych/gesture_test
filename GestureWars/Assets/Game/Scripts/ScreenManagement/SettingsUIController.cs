﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using PDollarGestureRecognizer;
using UnityEngine.UI;
using System;

public class SettingsUIController : DefaultScreenController
{
	[SerializeField] private GestureInputController gestureInput;
	[SerializeField] private LineRenderer gestureDrawer;
	[SerializeField] private RectTransform areaToShowGesture;
	private Vector2 gestureAreaSize;

	[SerializeField] private Transform EditButtons;
	[SerializeField] private Transform NewEditButtons;
	[SerializeField] private Button prevButton, nextButton;

	private List<Gesture> gestures;
	private int currentGesture = 0;




	void Start()
	{
		
		gestureAreaSize = areaToShowGesture.rect.size;
		gestureAreaSize = Camera.main.ScreenToWorldPoint(new Vector3(gestureAreaSize.x, gestureAreaSize.y, 10));

		gestures = GesturesManager.Instance.Gestures;

		if (gestures.Count > 0)
		{
			DrawGesture(gestures[0]);
		}
		else
		{
			ShowAddNewFigure();
		}


		UpdateButtons();
	}

	#region Buttons callbacks
	public void BackToMainMenu()
	{
		SceneManager.LoadScene(IDs.Scenes.MainMenu.GetNameString());
	}

	public void NextGesture(bool toRight)
	{
		if (toRight)
		{
			if (IsNextGestureExist(toRight))
			{
				currentGesture++;
				if (currentGesture < gestures.Count)
				{
					DrawGesture(gestures[currentGesture]);
				}
				else
				{
					ShowAddNewFigure();
				}
			}
		}
		else
		{
			if (IsNextGestureExist(toRight))
			{
				DrawGesture(gestures[--currentGesture]);
			}
		}
		UpdateButtons();
	}

	public void DeleteCurrentGesture()
	{
		GesturesManager.Instance.DeleteGesture(gestures[currentGesture]);
		UpdateAllButtons();
	}

	//not needed
	private void EditCurrentGesture()
	{
//		gestureInput.GetGesture((Gesture result) =>
//			{
//				GesturesManager.Instance.UpdateGesture(currentGesture, result);
//			});
	}

	#region implemented abstract members of DefaultScreenController
	public override void OnBackButtonPressed()
	{
		BackToMainMenu();
	}
	#endregion
	#endregion

	#region Settings Screen Logic
	public bool IsNextGestureExist(bool toRight)
	{
		if (toRight)
		{
			// gestures.Count because the last gesture will be to add
			return currentGesture < gestures.Count;
		}
		else
		{
			return currentGesture > 0;
		}
	}

	private void DrawGesture(Gesture gesture)
	{
		gestureInput.enabled = false;
		NewEditButtons.gameObject.SetActive(false);
		EditButtons.gameObject.SetActive(true);

		Vector3[] targetPoints = GesturesManager.GetAllVertices(gesture.Points, gestureAreaSize);
		gestureDrawer.SetVertexCount(targetPoints.Length);
		gestureDrawer.SetPositions(targetPoints);
	}

	private void UpdateButtons()
	{
		prevButton.interactable = IsNextGestureExist(false);
		nextButton.interactable = currentGesture < gestures.Count;
	}

	private void UpdateAllButtons()
	{
		currentGesture = currentGesture - 1;
		NextGesture(true);
	}

	private void ShowAddNewFigure()
	{
		NewEditButtons.gameObject.SetActive(true);
		EditButtons.gameObject.SetActive(false);

		gestureDrawer.SetVertexCount(0);

		gestureInput.GetGesture((Gesture result) =>
			{
				result.Name = gestures.Count + "-" +DateTime.Now.ToFileTime();
				Debug.Log("Save gesture ");
				GesturesManager.Instance.SaveGesture(result);
				UpdateAllButtons();
			});
	}
	#endregion
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuUIController : DefaultScreenController
{

	#region Button callbacks
	public void StartGame()
	{
		SceneManager.LoadScene(IDs.Scenes.Gameplay.GetNameString());
	}

	public void OpenSettings()
	{
		SceneManager.LoadScene(IDs.Scenes.Settings.GetNameString());
	}

	#region implemented abstract members of DefaultScreenController

	public override void OnBackButtonPressed()
	{
		Application.Quit();
	}

	#endregion
	#endregion
}

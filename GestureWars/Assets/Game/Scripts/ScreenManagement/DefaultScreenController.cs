﻿using UnityEngine;
using System.Collections;

public abstract class DefaultScreenController : MonoBehaviour
{
	// Update is called once per frame
	public virtual void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			OnBackButtonPressed();
		}
	}

	public abstract void OnBackButtonPressed();
}

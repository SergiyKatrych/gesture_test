﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LosePanelController : MonoBehaviour
{

	[SerializeField] private GameplayUIController gameplayController;
	[SerializeField] private Text scoreText;

	public void Show(int score)
	{
		gameObject.SetActive(true);
		scoreText.text = "score : " + score;
	}

	public void Replay()
	{
		gameplayController.Replay();
	}
}
